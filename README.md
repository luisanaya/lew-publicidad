# README #

Para utilizar este plugin se utilizan 2 shortcodes [lew_vertical_advertisign] y [lew_horizontal_advertisign]. Cada uno de ellos tiene atributos de shortcode de wordpress se puede utilizar vacío y utiliza los valores por defecto.

El banner vertical tiene por defecto:


```
#!php

'list' => 'banners-creacionpaginas-320.png|banners-disenoweb-320.png|banners-responsive-320.png|banners-sistemaweb-320.png',
'path' => plugins_url('', __FILE__) . '/imgs/'
```


Donde:
**"list"** es una lista de imágenes separadas por una barra vertical, al shortcode se le puede pasar el atributo list con la lista que se desee, solo los nombres.
**"path"** es la ubicación de las imágenes, por ejemplo: http://unaweb.com/wp-content/uploads/imagenes-publicidad/ y el plugin usará la lista y el path para mostrar una imagen aleatoriamente de list

El banner horizontal es responsive, los valores por defecto son:

```
#!php


'small-size' => 'banners-creacionpaginas-320.png|banners-disenoweb-320.png|banners-responsive-320.png|banners-sistemaweb-320.png',
'large-size' => 'banners-creacionpaginas-900.png|banners-disenoweb-900.png|banners-responsive-900.png|banners-sistemaweb-900.png',
'size-change' => '550',
'path' => plugins_url('', __FILE__) . '/imgs/'
```


Donde:
**"small-size"** es la lista de imágenes de tamaño pequeño (Width poco ancho), al igual que "list" del shortcode vertical se coloca la lista de imágenes.
**"large-size"** es la lista de imágenes de tamaño largo (Width ancho), se deben colocar en el mismo orden que la lista small ya que se asociarán por la posición. 
**"size-change"** es el ancho minimo de pantalla para que se produzca el cambio responsive, o sea, para hacer el cambio de imagen ancha a angosta.
**"path"** es la ubicación de las imágenes, por ejemplo: http://unaweb.com/wp-content/uploads/imagenes-publicidad/ y el plugin usará las listas y el path para mostrar una imagen aleatoriamente de las listas

### Acerca del repositorio ###

* Muestra una serie de banners publicitarios aleatoriamente de una lista
* V. 0.1
* [Aprenda más](https://bitbucket.org/tutorials/markdowndemo)

### Acerca del desarrollador ###

* Autor: Luis Anaya | La Élite Web
* Contacto: luisanaya@laeliteweb.com
* www.laeliteweb.com