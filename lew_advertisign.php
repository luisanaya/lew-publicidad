<?php
/*
Plugin Name: Publicidad La Élite Web
Description: Muestra una serie de banners publicitarios
Version: 0.1
Author: Luis Anaya | La Élite Web
Author URI: http://laeliteweb.com
*/

class lew_advertisign{
	function __construct(){
		add_shortcode('lew_vertical_advertisign', array($this,'vertical_advertisign'));
		add_shortcode('lew_horizontal_advertisign', array($this,'horizontal_advertisign'));
	}

	function vertical_advertisign($atts,$content){
		$atribbutes = shortcode_atts(array(
			'list' => 'banners-creacionpaginas-320.png|banners-disenoweb-320.png|banners-responsive-320.png|banners-sistemaweb-320.png',
			'path' => plugins_url('', __FILE__) . '/imgs/'
		), $atts);
		
		$imgs = explode('|',$atribbutes['list']);
		ob_start();
		?>
		
		<picture class="lew-advertisign-v">
			<?php for($i = 0; $i < count($imgs); $i++){ ?>
				<img src="<?php $atribbutes['path'].$imgs[$i]; ?>" title="Publicidad desarrollo web">
			<?php } ?>
		</picture>
		<?php
		return ob_get_clean();
	}
	
	function horizontal_advertisign($atts,$content){
		$atribbutes = shortcode_atts(array(
			'small-size' => 'banners-creacionpaginas-320.png|banners-disenoweb-320.png|banners-responsive-320.png|banners-sistemaweb-320.png',
			'large-size' => 'banners-creacionpaginas-900.png|banners-disenoweb-900.png|banners-responsive-900.png|banners-sistemaweb-900.png',
			'size-change' => '550',
			'path' => plugins_url('', __FILE__) . '/imgs/'
		), $atts);
		
		$imgsL = explode('|',$atribbutes['large-size']);
		$imgsS = explode('|',$atribbutes['small-size']);
		ob_start();
		?>
		
		<picture class="lew-advertisign-v">
			<?php for($i = 0; $i < count($imgsL); $i++){ ?>
				<source media="(max-width: <?php echo $atribbutes['large-size']; ?>px)" srcset=" <?php echo $atribbutes['path'].$imgsS[$i]; ?>">
				<img src="<?php $atribbutes['path'].$imgsL[$i]; ?>" title="Publicidad desarrollo web">
			<?php } ?>
		</picture>
		<?php
		return ob_get_clean();
	}
}

function lew_advertisign_init(){
	new lew_advertisign();
}
add_action('init','lew_advertisign_init',10);